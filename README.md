Key/Certificate routines taken from:
https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs

CSR - Cert Sign Request
CRL - Cert Revocation List

 README.md          - this file
 convert-der-pem    - convert DER to PEM format
 convert-pem-der    - convert PEM to DER format
 convert-pem-pkcs12 - convert PEM to PKCS12
 convert-pem-pkcs7  - convert PEM to PKCS7
 convert-pkcs12-pem - convert PKCS12 to PEM
 convert-pkcs7-pem  - convert PKCS7 to PEM
 create-csr         - create CSR
 create-csr-fromkey - create CSR from Key
 create-key         - create Key
 create-key-csr     - create Key CSR
 create-self-signed - create Self Signed Certificate
 create-self-signed-from-key - create Self Signed from Key
 create-self-signed-from-key-csr - create Self Signed from Key CSR
 decrypt-key        - decrypt Key
 encrypt-key        - encrypt Key
 keys               - keys directory
 params				- script parseters
 verify-cert-signedby-ca - verify Cert  is signed by CA Cert
 verify-key			- verify Key
 verify-key-matches-cert-csr - verify Key matches Cert CSR
 view-cert			- view Cert
 view-csr			- view CSR
